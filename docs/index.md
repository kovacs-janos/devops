# DevOps

![DevOps](img/devops-cycle.png)

Deliver value **faster** and more **reliable**.

 * rapid feedback
 * transparency
 * reduce risk and waste
 * shared responsibility (no silos)

 What are the **KPI**-s?
